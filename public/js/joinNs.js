function connect(nsName) {
    if (nsSocket) {
        //check to see if nsSocket is actually a socket
        nsSocket.close();
        //remove the eventLIstener before it's added again
        document.querySelector('#user-input').removeEventListener('submit', formSubmission);
    }
    nsSocket = io(`http://localhost:8000${nsName}`);
    nsSocket.on('nsRoomLoad', nsRooms => {
        //console.log(nsRooms);
        let rooms = document.getElementsByClassName('room-list')[0];
        rooms.innerHTML = '';
        nsRooms.forEach(room => {
            let glyph = (room.private) ? 'lock' : 'globe';
            rooms.innerHTML += `<li onclick="joinRoom('${room.roomTitle}')" ><p><span class='glyphicon glyphicon-${glyph}'></span>${room.roomTitle}</p></li>`
        });

    });

    nsSocket.on('messageToClients', msg => {
        console.log(msg);
        const newMsg = buildHTML(msg);
        document.querySelector('#messages').innerHTML += newMsg;
    });

    document.querySelector('.message-form').addEventListener('submit', formSubmission);
}

function formSubmission(event) {
    event.preventDefault();
    const newMessage = document.querySelector('#user-message').value;
    nsSocket.emit('newMessageToServer', { text: newMessage });
}

function buildHTML(msg) {
    const convertedDate = new Date(msg.time).toLocaleDateString();
    const newHTML = `
    <li>
        <div class="user-image">
            <img src="${ msg.avatar}" />
        </div>
        <div class="user-message">
            <div class="user-name-time">${ msg.username} <span>${convertedDate}</span></div>
            <div class="message-text">${ msg.text}</div>
        </div>
    </li>`
    return newHTML;
}