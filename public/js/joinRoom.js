function joinRoom(roomName) {
    //Send this roomName tot he server
    nsSocket.emit('joinRoom', roomName, newNumberOfMembers => {
        document.querySelector('.curr-room-num-users').innerHTML = `${newNumberOfMembers} <span class="glyphicon glyphicon-user"></span>`;
    });
    nsSocket.on('historyCatchUp', history => {
        //console.log(history)
        const messageUl = document.querySelector('#messages');
        messageUl.innerHTML = "";
        history.forEach(msg => {
            const newMsg = buildHTML(msg);
            const currentMessages = messageUl.innerHTML;
            messageUl.innerHTML = currentMessages + newMsg;
        })
        messageUl.scrollTo(0, messageUl.scrollHeight);
    });
    nsSocket.on('updateMembers', numMembers => {
        document.querySelector('.curr-room-num-users').innerHTML = `${numMembers} <span class="glyphicon glyphicon-user"></span>`
        document.querySelector('.curr-room-text').innerText = `${roomName}`
    })

    let searchBox = document.querySelector('#search-box');
    searchBox.addEventListener('input', event => {
        let messages = Array.from(document.getElementsByClassName('message-text'));
        messages.forEach(msg => {
            if (msg.innerText.toLowerCase().indexOf(event.target.value.toLowerCase()) === -1) {
                msg.style.display = 'none';
            } else {
                msg.style.display = 'block';
            }
        });
    });
}